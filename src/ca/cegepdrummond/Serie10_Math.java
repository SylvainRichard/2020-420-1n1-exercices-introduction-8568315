package ca.cegepdrummond;

import java.util.Scanner;

public class Serie10_Math {
    /*
     * 10A
     * Modifiez le code afin d'afficher la valeur minimale entrée
     */
    public void math1() {
        System.out.println("Veuillez entrer 2 chiffres svp.");
        Scanner s = new Scanner(System.in);
        int valeur1 = s.nextInt();
        int valeur2 = s.nextInt();

        System.out.println(Math.min(valeur1, valeur2));

    }

    /*
     * 10 B
     * Modifiez le code afin d'afficher la valeur maximale de la valeur absolue des entrées
     *
     * Exemple:
     * -3 -4
     * affichera: 4
     */
    public void math2() {
        Scanner s = new Scanner(System.in);
        int valeur1 = s.nextInt();
        int valeur2 = s.nextInt();

        valeur1 = Math.abs(valeur1);
        valeur2 = Math.abs(valeur2);

        System.out.println(Math.max(valeur1, valeur2));

    }
}
